# ANY Utils

## Overview

Collection of standalone utilities.

The contained packages have been tested under ROS Melodic and Ubuntu 18.04. This is research code, expect that it changes often and any fitness for a particular purpose is disclaimed.

The source code is released under a [BSD 3-Clause license](LICENSE).

Contact: leggedrobotics@ethz.ch

## Building

[![Build Status](https://ci.leggedrobotics.com/buildStatus/icon?job=bitbucket_leggedrobotics/any_utils/master)](https://ci.leggedrobotics.com/job/bitbucket_leggedrobotics/job/any_utils/job/master/)

In order to install, clone the latest version from this repository into your catkin workspace and compile the packages.

## Usage

Please report bugs and request features using the [Issue Tracker](https://bitbucket.org/leggedrobotics/any_utils/issues).

## Packages

This is only an overview. For more detailed documentation, please check the packages individually.

### color_tools

Simple color class for RGB applications.

### gtest_tools

Convenience classes for better printing of unit tests results.

### std_utils

Extensions of the C++ std library, such as compile time maps, enum arrays, etc.

### stopwatch

Stopwatch class including statistics for profiling execution time.

### tinyxml_tools

Parse and load parameters from XML files.

### yaml_tools

Edit yaml nodes, read and write yaml files.
